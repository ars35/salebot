/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A view showing a list of landmarks.
*/

import SwiftUI

//Отобрадение списка проектов
struct ProjectList: View {
    @EnvironmentObject var userData: UserData
    @State var showingProfile = false
    
    var profileButton: some View {
        Button(action: {self.showingProfile.toggle()}) {
            Image(systemName: "person.crop.circle")
                .imageScale(.large)
                .accessibility(label: Text("User Profile"))
        }
        .padding(.trailing)
    }
    
    var body: some View {
        NavigationView {
            List(self.userData.projects) { project in
                NavigationLink(destination: ChatList(projectName: project.name, id: project.id)) {
                    ProjectRow(project: project)
                }
            }
            .navigationBarTitle(Text("Проекты"))
            .navigationBarItems(trailing: profileButton)
            .sheet(isPresented: $showingProfile) {
                VStack(alignment: .center) {
                    URLImage(imageURL: self.userData.avatar)
                        .environmentObject(self.userData)
                        .frame(width: 150, height: 150)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.white, lineWidth: 4))
                        .shadow(radius: 10)
                        .padding(.top, -20)
                    Text(self.userData.login)
                        .font(.title)
                    Button(action: {
                        self.userData.showLoginView = true
                        self.showingProfile.toggle()
                    }) {
                           Text("Выйти")
                           .foregroundColor(Color.red)
                           .padding(.top, 20)
                        }
                }
            }
        }
        .onAppear() {
            //Загрузка списка проектов перед отображением View
            self.load(token: self.userData.token, login: self.userData.login)
        }
    }
    
    func load(token: String, login: String) {
        guard let url = URL(string: "https://salebot.pro/mobile_api_projects?user_token=\(token)&user_email=\(login)")
        else {
                fatalError("Incorrect URL.")
        }
    
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([Project].self, from: d)
                    DispatchQueue.main.async {
                        self.userData.projects = decodedLists
                    }
                }else {
                    print("No Data")
                }
            } catch {
                print ("Error")
            }
            
        }.resume()
         
    }
}
