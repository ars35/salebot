//
//  Chat.swift
//  Landmarks
//
//  Created by Арсений Баринов on 16.12.2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import SwiftUI

let msgs = [
    ChatMessage(id: 1, client_replica: true, answered: false, text: "12345"),
    ChatMessage(id: 2, client_replica: false, answered: true, text: "12345"),
    ChatMessage(id: 3, client_replica: true, answered: false, text: "12345"),
    ChatMessage(id: 4, client_replica: true, answered: false, text: "12345"),
    ChatMessage(id: 5, client_replica: false, answered: true, text: "12345")
]

struct ChatView: View {
    
    @State var finished = false
    @EnvironmentObject var userData: UserData

    var chatName: String
    var projectId: Int
    var chatId: Int
    var avatar: String
    @State private var  inputMessage: String = ""
    @State var idCount = 1
    
    //Обрабочик нажатия кнопки отправки сообщения
    func SendMessage() {
        //something
        guard let url = URL(string: "https://salebot.pro/mobile_api/\(projectId)/new_message") else {return}
        let parameters = ["client_id": "\(chatId)", "user_token": userData.token, "user_email": userData.login, "text": inputMessage]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {return}
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let error = error {
                print(error)
                return
            }
            
            if let response = response {
                print(response)
            }
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode < 200 || httpResponse.statusCode > 299
                {
                    //ОШИБКА!
                    return;
                }
            }
            else
            {
                return
            }
            
            guard let data = data else { return }
            print(data)
            
        }.resume()

    }
    
    var body: some View {
        //!!!Отображение сообщений в нужном порядке производится за счёт отражения сообщений и списка по вертикали
        //Не за счёт свойства reversed()!!!
        VStack {
            //Непосредственно отображение списка сообщений
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    //Сначала выводим отправленные, но не подтверждённые сообщения
                    ForEach(self.userData.pendingMessages) { currentMessage in
                        ChatMessageRow(currentMessage: currentMessage, avatar: self.avatar)
                            .scaleEffect(x: 1, y: -1, anchor: .center) //отражаем сообщение по горизонтали, это поможет вывести сообщения в правильном порядке
                    }
                    
                    //Последовательно выводим все сообщения
                    ForEach(self.userData.messages) { currentMessage in
                        ChatMessageRow(currentMessage: currentMessage, avatar: self.avatar)
                            .scaleEffect(x: 1, y: -1, anchor: .center) //отражаем сообщение по горизонтали, это поможет вывести сообщения в правильном порядке
                    }
                }
                .frame(width: UIScreen.main.bounds.width)
                .transition(.move(edge: .bottom))
            }
            .scaleEffect(x: 1, y: -1, anchor: .center) //отражаем весть ScrollView по горизонтали
            .padding(Edge.Set.bottom, -55)
           
            
            
            HStack {
                TextField("Сообщение", text: $inputMessage)
                    .frame(height: 10)
                    .padding()
                    .cornerRadius(20)
                Button(
                    action: {
                        self.SendMessage()
                        
                        self.userData.serialQueue.sync(flags:.barrier) {
                            self.send(text: &self.inputMessage, idCount: &self.idCount)
                            UIApplication.shared.endEditing()
                        }
                    }){
                        Image(systemName: "arrow.up.circle.fill")
                            .resizable()
                            .frame(width: 30, height: 30)
                            .aspectRatio(contentMode: ContentMode.fit)
                            .padding(Edge.Set.trailing, 5)
                    }
                }
        }
        .onTapGesture {
            UIApplication.shared.endEditing()
        }
        .keyboardResponsive()
        .navigationBarTitle(Text(self.chatName), displayMode: .inline)
        .onAppear() {
            
            //При необходимости (загружен другой чат) обнуляем список сообщений
            //и запоминаем id чата
            if(self.userData.currentChatId != self.chatId)
            {
                self.userData.messages.removeAll()
                self.userData.pendingMessages.removeAll()
                self.userData.currentChatId = self.chatId
            }
            self.load()
        }
    }
    
    func load() {

        guard let url = URL(string: "https://salebot.pro/mobile_api/\(self.projectId)/client/\(self.chatId).json?user_token=\(self.userData.token)&user_email=\(self.userData.login)&page=1")
        else {
                fatalError("Incorrect URL.")
        }
    
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([ChatMessage].self, from: d)
                    DispatchQueue.main.async {
                        self.userData.messages = decodedLists
                    }
                   
                }else {
                    print("No Data")
                }
            } catch {
                print ("Error")
            }
            
        }.resume()
         
    }
    
    func send(text: inout String, idCount: inout Int) {
        let msg: ChatMessage = ChatMessage(id: idCount, client_replica: false, answered: true, text: text)
        idCount += 1
        self.userData.pendingMessages.insert(msg, at: 0)
        text = ""
    }
}

struct ChatMessageRow : View {
    
    static let sentColor = Color(red: 244.0 / 255, green: 247.0 / 255, blue: 254.0 / 255)
    static let recvColor = Color(red: 31.0 / 255, green: 88.0 / 255, blue: 195.0 / 255)
    var currentMessage: ChatMessage
    var avatar: String
    
    var body: some View {
        Group {
            if currentMessage.client_replica == true {
                HStack (alignment: .bottom) {
                    Group {
                        URLImage(imageURL: avatar)
                            .aspectRatio(contentMode: ContentMode.fill)
                            .frame(width: 40, height: 40)
                            .clipShape(Circle())
                            .padding(.leading, 5)
                            .padding(.trailing, -5)
                        Text(currentMessage.text)
                            .padding(.top, 10)
                            .padding(.bottom, 10)
                            .padding(.leading, 10)
                            .padding(.trailing, 50)
                            .foregroundColor(Color.white)
                            .background(Self.recvColor)
                            .cornerRadius(20)
                        Text("11:35")
                            .font(.caption)
                            .padding(.leading, -50)
                            .foregroundColor(Color.gray)
                        Spacer()
                    }
                }
            } else {
                HStack (alignment: .bottom) {
                    Group {
                        Spacer()
                        Text(currentMessage.text)
                            .padding(.top, 10)
                            .padding(.bottom, 10)
                            .padding(.leading, 10)
                            .padding(.trailing, 50)
                            .foregroundColor(Color.black)
                            .background(Self.sentColor)
                            .cornerRadius(20)
                        Text("11:35")
                            .font(.caption)
                            //.padding(.top, 20)
                            .padding(.leading, -50)
                            .foregroundColor(Color.gray)
                        Image("LogoNoBack")
                            .aspectRatio(contentMode: ContentMode.fill)
                            .frame(width: 0, height: 40)
                            .clipShape(Circle())
                    }
                }
            }
        }
    }
}

struct Chat_Previews: PreviewProvider {
    
    static var previews: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack {
                //Последовательно выводим все сообщения
                ForEach(msgs) { currentMessage in
                    ChatMessageRow(currentMessage: currentMessage, avatar: "vk.com")
                }
            }
        }
    }
}
