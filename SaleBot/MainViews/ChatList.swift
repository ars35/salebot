//
//  ChatList.swift
//  Landmarks
//
//  Created by Арсений Баринов on 16.12.2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import SwiftUI

//Список чатов
struct ChatList: View {
    @EnvironmentObject var userData: UserData

    var projectName: String
    var id: Int
    
    var body: some View {
        List(self.userData.chats) { chat in
            if self.userData.isLoading
            {

            }
            else
            {
                NavigationLink(destination: ChatView(chatName: chat.name, projectId: self.id, chatId: chat.id, avatar: chat.avatar)) {
                    ChatRow(chat: chat)
                }
            }
        }
        .navigationBarTitle(Text(self.projectName), displayMode: .inline)
        .onAppear() {
            //При необходимости (загружен другой проект) обнуляем список чатов
            //и запоминаем id проекта
            if(self.userData.currentProjectId != self.id)
            {
                DispatchQueue.main.async {
                    self.userData.chats.removeAll()
                }
                self.userData.isLoading = true
                self.userData.currentProjectId = self.id
            }
            self.load(id: self.id, token: self.userData.token, login: self.userData.login)
        }
    }
    
    func load(id: Int, token: String, login: String) {
        guard let url = URL(string: "https://salebot.pro/mobile_api/\(id)/clients?id=\(id)&user_token=\(token)&user_email=\(login)")
        else {
                fatalError("Incorrect URL.")
        }
    
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([Chat].self, from: d)
                    DispatchQueue.main.async {
                        self.userData.chats = decodedLists
                        self.userData.isLoading = false
                    }
                }else {
                    print("No Data")
                }
            } catch {
                print ("Error")
            }
            
        }.resume()
         
    }
}

struct ChatList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone 7"], id: \.self) { deviceName in
            ChatList(projectName: "Имя проекта", id: 1)
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
