//
//  LoginPage.swift
//  Landmarks
//
//  Created by Арсений Баринов on 17.12.2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import SwiftUI
import Foundation
import ActionCableClient

//Структура для десериализации токена клиента
struct AuthToken: Decodable {
    let authentication_token: String
    let avatar: String
}
 
//Отображение индикатора бесконечной загрузки
struct ActivityIndicator: UIViewRepresentable {

    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style

    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

//Главный вид
struct MasterView: View {
    @EnvironmentObject var userData: UserData
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    static let lightFieldColor = Color(red: 244.0 / 255, green: 247.0 / 255, blue: 254.0 / 255)
    static let darkFieldColor = Color(red: 46.0 / 255, green: 46.0 / 255, blue: 46.0 / 255)
    
    @State private var login: String = ""
    @State private var password: String = ""
    
    //@State var a: Int = 0
    
    //Функция
//    func listen(task: URLSessionWebSocketTask)
//    {
//        task.receive {  result in
//            switch result {
//            case .failure(let error):
//                print("error: \(error)")
//            case .success(let message):
//                switch message {
//                case .string(let text):
//                    print("text: \(text)")
//                case .data(let data):
//                    print("data: \(data)")
//                @unknown default:
//                    fatalError()
//                }
//            }
//            self.listen(task: task)
//        }
//    }
    
    //Нить работы с webSockets и ActionCable
    func webSocketsThread()
    {
        guard let webSockUrl = URL(string: "wss://salebot.pro/cable?user_email=\(self.userData.login)&user_token=\(self.userData.token)")
            else {return}
        
        let client = ActionCableClient(url: webSockUrl)

        client.willConnect = {
            print("Will Connect")
        }
          
        client.onConnected = {
            print("Connected to \(client.url)")
        }
          
        client.onDisconnected = {(error: ConnectionError?) in
            print("Disconected with error: \(String(describing: error))")
        }
          
        client.willReconnect = {
            print("Reconnecting to \(client.url)")
            return true
        }
          
        let channel = client.create("NotifierChannel")
        channel.onSubscribed = {
            print("Subscribed to NotifierChannel")
        }
        
        //Непосредственно приём сообщений по ActionCable
        channel.onReceive = {(data: Any?, error: Error?) in
            if let _ = error {
                print(error as Any)
                return
            }
            
            if let _ = data {
                
                print(data as Any)

                let message = data as! [String : String]
                if message["message"] == nil
                {
                    return
                }
                //Необходимо заменить экранированный слеш на обычный, чтобы корректно распарсить JSON
                let messageString = message["message"]!.replacingOccurrences(of: "\\\"", with: "\"", options: .literal, range: nil)
                let messageData = messageString.data(using: .utf8)!
                
                var decodedMessage: MessageFromCable
                do {
                    decodedMessage = try JSONDecoder().decode(MessageFromCable.self, from: messageData)
                    print(decodedMessage)
                } catch {
                    print ("Error")
                    return
                }
                
                //Здесь мы проверяем, если пришедшее соощение - подтверждение отправленного,
                //то удаляем его из списка pending и помещаем в обычный список сообщений
                if decodedMessage.client_id == self.userData.currentChatId
                {
                    let client_replica = decodedMessage.type == "message_from_client" ? true : false
                    let msg: ChatMessage = ChatMessage(
                        id: decodedMessage.id,
                        client_replica: client_replica,
                        answered: true,
                        text: decodedMessage.message)
                    
                    if !client_replica
                    {
                        self.userData.serialQueue.sync(flags:.barrier) {
                            for curMessage in self.userData.pendingMessages.startIndex..<self.userData.pendingMessages.endIndex {
                                let currentMessage = self.userData.pendingMessages[curMessage]
                                if currentMessage.text == decodedMessage.message {
                                    self.userData.pendingMessages.remove(at: curMessage)
                                    break;
                                }
                            }
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.userData.messages.insert(msg, at: 0)
                    }
                }
                
            }
        }
        
        client.connect()
    }
    
    //Обработчик нажатия кнопки "Вход".
    func submit() {

        guard let url = URL(string: "https://salebot.pro/users/sign_in") else {return}
        let parameters = ["user": ["email": login, "password": password]]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {return}
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let error = error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self.userData.showWait = false
                    self.userData.showLoginView = true
                }
                return
            }
            
            if let response = response {
                print(response)
            }
            
            if let httpResponse = response as? HTTPURLResponse
            {
                if httpResponse.statusCode < 200 || httpResponse.statusCode > 299
                {
                    DispatchQueue.main.async {
                        self.userData.showWait = false
                        self.userData.showLoginView = true
                    }
                    return;
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.userData.showWait = false
                    self.userData.showLoginView = true
                }
                return
            }
            
            guard let data = data else { return }
            print(data)
            
            do {
                
                let authToken = try JSONDecoder().decode(AuthToken.self, from: data)
                print(authToken.authentication_token)
                
                //Запомним токен
                DispatchQueue.main.async {
                    //Запоминаем логин и токен пользователя
                    self.userData.login = self.login
                    self.userData.token = authToken.authentication_token
                    if authToken.avatar != ""
                    {
                        self.userData.avatar = authToken.avatar
                    }
                }
                
                guard let json = try JSONSerialization.jsonObject(with: data, options: [])
                    as? [String : Any] else { return }
                print(json)
                
            } catch {
                print(error)
            }
            
            DispatchQueue.main.async {
                //Меняем флаги для отображения основного View
                self.userData.showWait = false
                self.userData.showLoginView = false
            }
            
            //Вешаемся на WebSockets для получения входящих сообщений
            self.webSocketsThread()
            
        }.resume()
        
        self.userData.showWait = true
    }

    var body: some View {
        VStack {
            if !self.userData.showLoginView {
                ProjectList()
            } else {
                VStack {
                    Image("LogoNoBack")
                        .resizable()
                        .aspectRatio(contentMode: ContentMode.fit)
                        .frame(width: 150, height: 150)
                        //.padding(Edge.Set.bottom, 20)
                    /*Text("SaleBot")
                        .bold()
                        .font(.title)
                    Text("Конструктор чат-ботов для популярных мессенджеров, социальных сетей и онлайн чатов")
                        .multilineTextAlignment(.center)
                        .font(.body)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 20, trailing: 0))*/
                    TextField("Email", text: $login) {
                        UIApplication.shared.endEditing()
                    }
                        .keyboardType(.emailAddress)
                        .textContentType(.username)
                        .padding()
                        .background(self.colorScheme == .light ? Self.lightFieldColor : Self.darkFieldColor)
                        .foregroundColor(self.userData.showWait ? Color.gray : Color.primary)
                        .cornerRadius(4)
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 15, trailing: 0))
                        .disabled(self.userData.showWait)
                        .tag(1)
                    SecureField("Password", text: $password) {
                        UIApplication.shared.endEditing()
                    }
                        .textContentType(.password)
                        .padding()
                        .background(self.colorScheme == .light ? Self.lightFieldColor : Self.darkFieldColor)
                        .foregroundColor(self.userData.showWait ? Color.gray : Color.primary)
                        .cornerRadius(4.0)
                        .padding(.bottom, 10)
                        .disabled(self.userData.showWait)
                        .tag(2)
                    Button(
                        action: {
                            if !self.password.isEmpty
                            {
                                UIApplication.shared.endEditing()
                                self.submit()
                            }
                            else
                            {
                                
                            }
                        }){
                        HStack(alignment: .center) {
                            Spacer()
                            
                            if self.userData.showWait
                            {
                                ActivityIndicator(isAnimating: .constant(true), style: .medium)
                            }
                            else
                            {
                                Text("Вход")
                                    .foregroundColor(Color.white)
                                    .bold()
                            }
                            
                            Spacer()
                        }
                    }
                    .padding()
                    .background(self.userData.showWait ? Color.gray : Color.green)
                    .cornerRadius(4)
                    .disabled(self.userData.showWait)
                }
                .padding()
                .keyboardResponsive()
                .onTapGesture {
                    UIApplication.shared.endEditing()
                }
            }
        }
        .animation(.easeInOut)
    }
}

struct LoginPage_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone 7", "iPhone se", "iPhone 11"], id: \.self) { deviceName in
           MasterView()
            .environmentObject(UserData())
            .previewDevice(PreviewDevice(rawValue: deviceName))
            .previewDisplayName(deviceName)
        }
        
    }
}
