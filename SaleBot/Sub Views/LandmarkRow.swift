/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A single row to be displayed in a list of landmarks.
*/

import SwiftUI

struct ProjectRow: View {
    var project: Project

    var body: some View {
        HStack {
            Text(verbatim: project.name)
            Spacer()
        }
    }
}

/*
struct LandmarkRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LandmarkRow(project: projectData[0])
            LandmarkRow(project: projectData[1])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
*/
