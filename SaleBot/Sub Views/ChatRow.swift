//
//  ChatRow.swift
//  Landmarks
//
//  Created by Арсений Баринов on 19.12.2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import SwiftUI

struct ChatRow: View {
    @EnvironmentObject var userData: UserData
    var chat: Chat

    var body: some View {
        HStack {
            URLImage(imageURL: self.chat.avatar)
                .frame(width: 70, height: 70)
            VStack (alignment: .leading) {
                Text(verbatim: self.chat.name)
                    .bold()
                HStack {
                    if self.chat.last_message.answered == true {
                        URLImage(imageURL: self.userData.avatar)
                            .frame(width: 20, height: 20)
                        Text(self.chat.last_message.text)
                            .font(.subheadline)
                            .lineLimit(1)
                            .frame(height: 20)
                    } else {
                        Text(self.chat.last_message.text)
                            .font(.subheadline)
                            .lineLimit(1)
                            .frame(height: 40)
                    }
                }
            }
            Spacer()
        }
    }
}

//struct ChatRow_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            ChatRow(landmark: landmarkData[0])
//            ChatRow(landmark: landmarkData[1])
//        }
//        .previewLayout(.fixed(width: 300, height: 70))
//    }
//}
