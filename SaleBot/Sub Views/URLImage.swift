/*
Файл View для загрузки изображения по URL
 */


import SwiftUI


//TODO: он каждый раз грузит картинку? переделать?
class URLImageLoader: ObservableObject {
    //var didChange = PassthroughSubject<Data, Never>()
    
    @ Published var data = Data()
    
    init(imageURL: String) {
        guard let url = URL(string: imageURL) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            
            print(imageURL)
            
            DispatchQueue.main.async {
                self.data = data
            }
        }.resume()
    }
}

struct URLImage: View {
    //@ObservedObject var imageLoader: URLImageLoader
    @EnvironmentObject var userData: UserData
    
    var imageUrl: String
    
    init(imageURL: String) {
        self.imageUrl = imageURL
    }
    
    func load() {
        //Проверим, не загружено ли уже изображение
        if self.userData.avatars[imageUrl] == nil {
            self.userData.avatars[imageUrl] = Data()
        } else {
            return
        }
        guard let url = URL(string: self.imageUrl) else {return}
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {return}
            
            print(self.imageUrl)
            
            DispatchQueue.main.async {
                self.userData.avatars[self.imageUrl]! = data
            }
        }.resume()
    }
    
    var body: some View {
        Image(uiImage: (self.userData.avatars[self.imageUrl] == nil || self.userData.avatars[self.imageUrl]!.isEmpty) ? UIImage(imageLiteralResourceName: "Logo.jpg") : UIImage(data: self.userData.avatars[self.imageUrl]!)!)
            .resizable()
            .clipShape(Circle())
            .aspectRatio(contentMode: ContentMode.fill)
        .onAppear() {
            self.load()
        }
    }
    
//    var body: some View {
//           Image(uiImage: (imageLoader.data.isEmpty) ? UIImage(imageLiteralResourceName: "Logo.jpg") : UIImage(data: imageLoader.data)!)
//               .resizable()
//               //.frame(width: 70, height: 70)
//               .clipShape(Circle())
//               .aspectRatio(contentMode: ContentMode.fill)
//       }
}



