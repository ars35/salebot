/*
Функции загрузки массива чатов с сайта
*/

import UIKit
import SwiftUI
import CoreLocation

struct Message: Hashable, Decodable, Identifiable {
    var id: Int
    var client_replica: Bool
    var answered: Bool
    var text: String
}

struct MessageFromCable: Hashable, Decodable, Identifiable {
    var id: Int
    var delivered: Int
    var type: String
    var message: String
    var client_id: Int
    
    /*"id":21848722,
    "delivered":1,
    "error_message":"true",
    "type":"message_from_client",
    "message":"test",
    "avatar":"https://sun9-53.userapi.com/c622225/v622225909/3b8f7/S-PPx-XT--M.jpg?ava=1",
    "name":"Арсений Баринов",
    "client_type":0,
    "time_unix":1583017595,
    "date":"02:06 01.03.20",
    "project_id":1,
    "project_name":"Лидератор",
    "client_id":1182099,
    "show_notifications":true*/
}

struct Chat: Hashable, Decodable, Identifiable {
    var id: Int
    var name: String
    var last_message: Message
    var avatar: String
}

public class Chats: ObservableObject {
    
    @EnvironmentObject var userData: UserData
    //@Published var chats = [Chat]()
    
//    init(token: String, login: String) {
//        load(token: token, login: login)
//    }
    
    func load(id: Int, token: String, login: String) {
        //https://salebot.pro/mobile_api/9848/clients?id=9848&user_token=4YN7cZz2F7_YSkSPd94z&user_email=arseny35barinov@gmail.com
        guard let url = URL(string: "https://salebot.pro/mobile_api/\(id)/clients?id=\(id)&user_token=\(token)&user_email=\(login)")
        else {
                fatalError("Incorrect URL.")
        }
    
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([Chat].self, from: d)
                    DispatchQueue.main.async {
                        self.userData.chats = decodedLists
                    }
                }else {
                    print("No Data")
                }
            } catch {
                print ("Error")
            }
            
        }.resume()
         
    }
}
