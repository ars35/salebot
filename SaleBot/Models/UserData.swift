//
//  UserData.swift
//  Landmarks
//
//  Created by Арсений Баринов on 18.12.2019.
//  Copyright © 2019 Apple. All rights reserved.
//

import SwiftUI
import Combine

final class UserData: ObservableObject  {
    @Published var showLoginView = true
    @Published var showWait = false
    
    @Published var login: String = ""
    @Published var token: String = ""
    @Published var avatar: String = ""
    
    @Published var isLoading: Bool = true
    
    var currentChatId: Int = 0
    var currentProjectId: Int = 0
    
    let serialQueue = DispatchQueue(label: "com.chatSerialQueue", attributes: .concurrent)
    
    
    @Published var projects = [Project]()
    @Published var chats = [Chat]()
    @Published var messages = [ChatMessage]()
    @Published var pendingMessages = [ChatMessage]()
    
    @Published var avatars: [String : Data] = [:]
}
