/*
Функции загрузки массива сообщения чата с сайта
*/

import UIKit
import SwiftUI
import CoreLocation

struct ChatMessage: Hashable, Decodable, Identifiable {
    var id: Int
    var client_replica: Bool
    var answered: Bool
    var text: String
}

public class Messages: ObservableObject {
    
    var idCount = 1
    
//    func send(text: inout String) {
//        let msg: ChatMessage = ChatMessage(id: idCount, client_replica: false, answered: true, text: text)
//        idCount += 1
//        self.messages.insert(msg, at: 0)
//        text = ""
//    }
//    
//    func load(projectId: Int, chatId: Int, token: String, login: String) {
//        /*https://salebot.pro/mobile_api/9848/client/887405.json?user_token=4YN7cZz2F7_YSkSPd94z&user_email=arseny35barinov@gmail.com&page=1*/
//        guard let url = URL(string: "https://salebot.pro/mobile_api/\(projectId)/client/\(chatId).json?user_token=\(token)&user_email=\(login)&page=1")
//        else {
//                fatalError("Incorrect URL.")
//        }
//    
//        URLSession.shared.dataTask(with: url) {(data,response,error) in
//            do {
//                if let d = data {
//                    let decodedLists = try JSONDecoder().decode([ChatMessage].self, from: d)
//                    DispatchQueue.main.async {
//                        self.messages = decodedLists
//                    }
//                }else {
//                    print("No Data")
//                }
//            } catch {
//                print ("Error")
//            }
//            
//        }.resume()
//         
//    }
}
