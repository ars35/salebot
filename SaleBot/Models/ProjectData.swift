/*
Функции загрузки массива проектов с сайта
*/

import UIKit
import SwiftUI
import CoreLocation

struct Project: Hashable, Decodable, Identifiable {
    var id: Int
    var name: String
}

public class Projects: ObservableObject {
    
    @EnvironmentObject var userData: UserData
    //@Published var projects = [Project]()
    
//    init(token: String, login: String) {
//        load(token: token, login: login)
//    }
    
    func load(token: String, login: String) {
        guard let url = URL(string: "https://salebot.pro/mobile_api_projects?user_token=\(token)&user_email=\(login)")
        else {
                fatalError("Incorrect URL.")
        }
    
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            do {
                if let d = data {
                    let decodedLists = try JSONDecoder().decode([Project].self, from: d)
                    DispatchQueue.main.async {
                        self.userData.projects = decodedLists
                    }
                }else {
                    print("No Data")
                }
            } catch {
                print ("Error")
            }
            
        }.resume()
         
    }
}


/*func loadProjectList<T: Decodable>() -> T {
    
    let data: Data
    
    guard let url = URL(string: "https://salebot.pro/mobile_api_projects?user_token=4YN7cZz2F7_YSkSPd94z&user_email=arseny35barinov@gmail.com")
        else {
            fatalError("Incorrect URL.")
    }
    let request = URLRequest(url: url)
    let session = URLSession.shared
    
    session.dataTask(with: request) { (data, response, error) in
        
        if let error = error {
            print(error)
            return
        }
        
        if let response = response {
            print(response)
        }
        
        if let httpResponse = response as? HTTPURLResponse
        {
            //TODO: на самом деле, успешный статус - это не только 201
            if httpResponse.statusCode < 200 || httpResponse.statusCode > 299
            {
                return;
            }
        }
        else
        {
            return
        }
        
        guard let data = data else { return }
        print(data)
        
        do {
            print(projectData)
            let decoder = JSONDecoder()
            projectData = try decoder.decode([Project].self, from: data)
            print(projectData)
            
        } catch {
            print(error)
        }
    }.resume()
    
    let filename: String = "projectData.json"
    
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}*/
